import moment from 'moment';
import Api from '../../lib/api';
import model from '../../models/tasks';
import config from '../../constants/config';


/**
 * Mocks
 */
jest.mock('axios');
afterEach(jest.resetAllMocks);

const mockData = {
  id: '1',
  name: 'name',
  subtitle: 'subtitle',
  description: 'description',
  tags: [],
};

/**
 * Tests
 */
it('Tasks fetchList() work correctly', async () => {
  Api.get.mockResolvedValue({ data: mockData });
  const initialState = { tasks: { list: [], lastSync: null } };
  const dispatch = { tasks: { replace: jest.fn((res) => res) } };

  await model.effects(dispatch).fetchList({}, initialState).then(() => {
    expect(Api.get).toHaveBeenCalledWith(config.apiEndpoints.tasksGetList);
    expect(dispatch.tasks.replace).toHaveBeenCalledTimes(1);
  });
});

it('Tasks fetchList() does not go to API if lastSync just set', async () => {
  const initialState = { tasks: { lastSync: moment().format() } };
  await model.effects().fetchList({}, initialState).then((res) => expect(res).toEqual(true));
});

it('Tasks Model returns correctly', () => {
  expect(model.reducers.replace({}, {
    data: [mockData],
  })).toMatchObject({
    list: [mockData],
    lastSync: {},
  });
});
