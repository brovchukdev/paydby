import moment from 'moment';
import Api from '../../lib/api';
import model from '../../models/posts';
import config from '../../constants/config';


/**
 * Mocks
 */
jest.mock('axios');
afterEach(jest.resetAllMocks);

const mockData = {
  id: '1',
  name: 'name',
  order: 1,
  subtitle: 'subtitle',
  content: 'content',
  image: 'image',
};

/**
 * Tests
 */
it('Posts fetchList() work correctly', async () => {
  Api.get.mockResolvedValue({ data: mockData });
  const initialState = { posts: { list: [], lastSync: null } };
  const dispatch = { posts: { replace: jest.fn((res) => res) } };

  await model.effects(dispatch).fetchList({}, initialState).then(() => {
    expect(Api.get).toHaveBeenCalledWith(config.apiEndpoints.postsGetList);
    expect(dispatch.posts.replace).toHaveBeenCalledTimes(1);
  });
});

it('Posts fetchList() does not go to API if lastSync just set', async () => {
  const initialState = { posts: { lastSync: moment().format() } };
  await model.effects().fetchList({}, initialState).then((res) => expect(res).toEqual(true));
});

it('Posts Model returns correctly', () => {
  expect(model.reducers.replace({}, {
    data: [mockData],
  })).toMatchObject({
    list: [mockData],
    lastSync: {},
  });
});
