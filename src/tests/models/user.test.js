import Api from '../../lib/api';
import model from '../../models/user';
import config from '../../constants/config';


/**
 * Mocks
 */
jest.mock('axios');
afterEach(jest.resetAllMocks);

const mockData = {
  id: '1',
  firstName: 'First name',
  lastName: 'Last name',
  email: 'test@email.com',
  avatar: '',
};

/**
 * Tests
 */
it('User fetchData() work correctly', async () => {
  Api.get.mockResolvedValue({ data: mockData });
  const dispatch = { user: { saveData: jest.fn((res) => res) } };

  await model.effects(dispatch).fetchData().then(() => {
    expect(Api.get).toHaveBeenCalledWith(config.apiEndpoints.userGetData);
    expect(dispatch.user.saveData).toHaveBeenCalledTimes(1);
  });
});

it('User Model returns correctly', () => {
  expect(model.reducers.saveData({}, {
    user: mockData,
  })).toMatchObject({
    id: '1',
    firstName: 'First name',
    lastName: 'Last name',
    email: 'test@email.com',
    avatar: '',
  });
});
