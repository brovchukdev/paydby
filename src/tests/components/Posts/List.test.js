import React from 'react';
import renderer from 'react-test-renderer';
import { render } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import PostsList from '../../../components/Posts/List';


it('<PostsList /> renders correctly', () => {
  const Component = (
    <MemoryRouter>
      <PostsList />
    </MemoryRouter>
  );

  // Matches snapshot
  const tree = renderer.create(Component).toJSON();
  expect(tree).toMatchSnapshot();

  // Has the correct text on the page
  const { getByText } = render(Component);
  expect(getByText('No Posts found')).toBeInTheDocument();
});

it('<PostsList /> shows a list when provided', () => {
  const Component = (
    <MemoryRouter>
      <PostsList
        posts={[{
          id: '123',
          name: 'test',
          order: 1,
          subtitle: 'subtitle test',
          content: 'content',
          image: 'image',
        }]}
      />
    </MemoryRouter>
  );

  // Matches snapshot
  expect(renderer.create(Component).toJSON()).toMatchSnapshot();

  // Has the correct text on the page
  const { getByText } = render(Component);
  expect(getByText(/test/)).toBeInTheDocument();
});
