import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';
import PostSingle from '../../../components/Posts/Single';


it('<PostSingle /> renders correctly', () => {
  const Component = (
    <MemoryRouter>
      <PostSingle
        post={{
          id: '123',
          name: 'test',
          order: 1,
          subtitle: 'subtitle test',
          content: 'content',
          image: 'image',
        }}
      />
    </MemoryRouter>
  );

  // Matches snapshot
  expect(renderer.create(Component).toJSON()).toMatchSnapshot();
});
