import React from 'react';
import renderer from 'react-test-renderer';
import { render } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import TasksList from '../../../components/Tasks/List';


it('<TasksList /> renders correctly', () => {
  const Component = (
    <MemoryRouter>
      <TasksList />
    </MemoryRouter>
  );

  // Matches snapshot
  const tree = renderer.create(Component).toJSON();
  expect(tree).toMatchSnapshot();

  // Has the correct text on the page
  const { getByText } = render(Component);
  expect(getByText('No Tasks found')).toBeInTheDocument();
});

it('<TasksList /> shows a list when provided', () => {
  const Component = (
    <MemoryRouter>
      <TasksList
        tasks={[{
          id: '123',
          name: 'test',
          subtitle: 'subtitle test',
          description: 'description',
          tags: [],
        }]}
      />
    </MemoryRouter>
  );

  // Matches snapshot
  expect(renderer.create(Component).toJSON()).toMatchSnapshot();
});
