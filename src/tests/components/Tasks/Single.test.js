import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';
import TaskSingle from '../../../components/Tasks/Single';


it('<TaskSingle /> renders correctly', () => {
  const Component = (
    <MemoryRouter>
      <TaskSingle
        task={{
          id: '123',
          name: 'test',
          subtitle: 'subtitle test',
          description: 'description',
          tags: [],
        }}
      />
    </MemoryRouter>
  );

  // Matches snapshot
  expect(renderer.create(Component).toJSON()).toMatchSnapshot();
});
