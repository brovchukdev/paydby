import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';
import Spinner from '../../../components/UI/Spinner';


it('<Spinner /> renders correctly', () => {
  const Component = <MemoryRouter><Spinner /></MemoryRouter>;

  // Matches snapshot
  const tree = renderer.create(Component).toJSON();
  expect(tree).toMatchSnapshot();
});
