import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';
import Header from '../../../components/UI/Header';


it('<Header /> renders correctly', () => {
  const Component = (
    <MemoryRouter>
      <Header />
    </MemoryRouter>
  );

  // Matches snapshot
  const tree = renderer.create(Component).toJSON();
  expect(tree).toMatchSnapshot();
});
