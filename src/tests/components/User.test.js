import React from 'react';
import { render } from '@testing-library/react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';
import UserInfo from '../../components/User/Info';


it('<UserInfo /> renders correctly', () => {
  const Component = (
    <MemoryRouter>
      <UserInfo user={
        {
          id: '1', firstName: 'Test', lastName: 'Test', email: 'test@email.com', avatar: '',
        }
      }
      />
    </MemoryRouter>
  );

  // Matches snapshot
  const tree = renderer.create(Component).toJSON();
  expect(tree).toMatchSnapshot();

  // Has the correct text on the page
  const { getByText } = render(Component);
  expect(getByText(/Name: Test/)).toBeInTheDocument();
  expect(getByText(/Last name: Test/)).toBeInTheDocument();
  expect(getByText(/Email: test@email.com/)).toBeInTheDocument();
});
