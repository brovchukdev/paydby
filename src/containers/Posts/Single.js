import React, { useState, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PostType from '../../prop-types/post';

// Components
import Layout from '../../components/Posts/Single';

const PostsSingleContainer = ({ posts, postId, fetchPosts }) => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetchPosts().catch(({ message }) => setError(message)).finally(() => setLoading(false));
  }, [fetchPosts]);

  const post = useMemo(() => posts.find(({ id }) => postId === id), [posts, postId]);

  return (
    <Layout
      loading={loading}
      error={error}
      post={post}
    />
  );
};

PostsSingleContainer.propTypes = {
  posts: PropTypes.arrayOf(PostType), // eslint-disable-line
  postId: PropTypes.string, // eslint-disable-line
  fetchPosts: PropTypes.func.isRequired,
};

const mapStateToProps = ({ posts: { list } }) => ({ posts: list });

const mapDispatchToProps = (dispatch) => ({
  fetchPosts: dispatch.posts.fetchList,
});

export default connect(mapStateToProps, mapDispatchToProps)(PostsSingleContainer);
