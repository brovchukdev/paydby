import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PostType from '../../prop-types/post';

// Components
import Layout from '../../components/Posts/List';

const PostsListContainer = ({ posts, fetchPosts }) => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetchPosts().catch(({ message }) => setError(message)).finally(() => setLoading(false));
  }, [fetchPosts]);

  return (
    <Layout
      loading={loading}
      error={error}
      posts={posts}
    />
  );
};

PostsListContainer.propTypes = {
  posts: PropTypes.arrayOf(PostType), // eslint-disable-line
  fetchPosts: PropTypes.func.isRequired,
};

const mapStateToProps = ({ posts: { list } }) => ({ posts: list });

const mapDispatchToProps = (dispatch) => ({
  fetchPosts: dispatch.posts.fetchList,
});

export default connect(mapStateToProps, mapDispatchToProps)(PostsListContainer);
