import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TaskType from '../../prop-types/task';

// Components
import Layout from '../../components/Tasks/List';

const TasksListContainer = ({ tasks, fetchTasks }) => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetchTasks().catch(({ message }) => setError(message)).finally(() => setLoading(false));
  }, [fetchTasks]);

  return (
    <Layout
      loading={loading}
      error={error}
      tasks={tasks}
    />
  );
};

TasksListContainer.propTypes = {
  tasks: PropTypes.arrayOf(TaskType), // eslint-disable-line
  fetchTasks: PropTypes.func.isRequired,
};

const mapStateToProps = ({ tasks: { list } }) => ({ tasks: list });

const mapDispatchToProps = (dispatch) => ({
  fetchTasks: dispatch.tasks.fetchList,
});

export default connect(mapStateToProps, mapDispatchToProps)(TasksListContainer);
