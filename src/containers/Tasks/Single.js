import React, { useState, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TaskType from '../../prop-types/task';

// Components
import Layout from '../../components/Tasks/Single';

const TasksSingleContainer = ({ tasks, taskId, fetchTasks }) => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetchTasks().catch(({ message }) => setError(message)).finally(() => setLoading(false));
  }, [fetchTasks]);

  const task = useMemo(() => tasks.find(({ id }) => taskId === id), [tasks, taskId]);

  return (
    <Layout
      loading={loading}
      error={error}
      task={task}
    />
  );
};

TasksSingleContainer.propTypes = {
  tasks: PropTypes.arrayOf(TaskType), // eslint-disable-line
  taskId: PropTypes.string, // eslint-disable-line
  fetchTasks: PropTypes.func.isRequired,
};

const mapStateToProps = ({ tasks: { list } }) => ({ tasks: list });

const mapDispatchToProps = (dispatch) => ({
  fetchTasks: dispatch.tasks.fetchList,
});

export default connect(mapStateToProps, mapDispatchToProps)(TasksSingleContainer);
