// Home
export { default as Home } from './Home';

// Tasks
export { default as TasksList } from './Tasks/List';
export { default as TasksSingle } from './Tasks/Single';

// Posts
export { default as PostsList } from './Posts/List';
export { default as PostsSingle } from './Posts/Single';
