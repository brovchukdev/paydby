import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import Routes from '../routes';
import config from '../constants/config';

// Components
import { Spinner } from '../components/UI';
import NothingTemplate from '../components/Templates/Nothing';


const MainContainer = ({ fetchUser }) => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetchUser().catch(({ message }) => setError(message)).finally(() => setLoading(false));
  }, [fetchUser]);

  if (loading) {
    return <Spinner />;
  }

  if (error) {
    return (
      <NothingTemplate pageTitle={`${config.appName} - Error`}>
        Oops, something went wrong.
        <pre>{error}</pre>
      </NothingTemplate>
    );
  }

  return (
    <Router>
      <Routes />
    </Router>
  );
};

MainContainer.propTypes = {
  fetchUser: PropTypes.func.isRequired,
};

const mapStateToProps = null;

const mapDispatchToProps = (dispatch) => ({
  fetchUser: dispatch.user.fetchData,
});

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer);
