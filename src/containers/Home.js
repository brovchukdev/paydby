import React from 'react';
import { connect } from 'react-redux';
import UserType from '../prop-types/user';

// Components
import Layout from '../components/Home';

const HomeContainer = ({ user }) => <Layout user={user} />;

HomeContainer.propTypes = {
  user: UserType, // eslint-disable-line
};

const mapStateToProps = ({ user }) => ({
  user: user.id ? user : null,
});

export default connect(mapStateToProps)(HomeContainer);
