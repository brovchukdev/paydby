/* global window */
const { host } = window.location;

/**
 * Environments
 */
let env = 'production'; // Defaults to production
if (process.env.NODE_ENV === 'development' || host.includes('local')) env = 'development';
if (host.includes('stage.')) env = 'stage';

/**
 * Config object to export
 */
export default {
  // App Details
  appName: 'PaydByTest',

  // Build Configuration - eg. Debug or Release?
  isDevEnv: (env === 'development'),
  ENV: env,

  // Date Format
  dateFormat: 'Do MMM YYYY',

  // API
  apiBaseUrl: (env === 'production')
    ? 'https://www.mocky.io'
    : 'https://www.mocky.io',

  cacheTimeout: {
    amount: 15,
    unit: 'minutes',
  },

  apiEndpoints: {
    userGetData: '/v2/5b9751e5300000332a0bd52d',
    tasksGetList: '/v2/5b97533d30000070000bd533',
    postsGetList: '/v2/5b9755c43000006a000bd53f',
  },
};
