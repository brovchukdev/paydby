import React from 'react';
import { Switch, Route } from 'react-router-dom';
import CustomRoute from './Route';

// Containers
import {
  Home,
  TasksList,
  TasksSingle,
  PostsList,
  PostsSingle,
} from '../containers';

// Components
import Error from '../components/UI/Error';

/**
 * All of the routes
 */
const Index = () => (
  <Switch>

    {/* Dashboard */}
    <CustomRoute path="/" exact component={Home} />

    {/* Tasks */}
    <CustomRoute path="/tasks" exact component={TasksList} />
    <CustomRoute path="/tasks/:taskId" component={TasksSingle} />

    {/* Posts */}
    <CustomRoute path="/posts" exact component={PostsList} />
    <CustomRoute exact path="/posts/:postId" component={PostsSingle} />

    {/* 404 */}
    <Route
      render={(props) => (
        <Error {...props} title="404" content="Sorry, the route you requested does not exist" />
      )}
    />
  </Switch>
);

export default Index;
