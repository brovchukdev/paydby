import moment from 'moment';
import initialState from '../store/tasks';
import HandleErrorMessage from '../lib/format-error-messages';
import Api from '../lib/api';
import config from '../constants/config';

/**
 * Transform the endpoint data structure into our redux store format
 * @param {object} item
 */
const transform = (item) => ({
  id: item.id,
  name: item.name,
  subtitle: item.subtitle,
  description: item.description,
  tags: Array.isArray(item.tags) ? item.tags : [],
});

export default {
  namespace: 'tasks',

  /**
   * Initial state
   */
  state: initialState,

  /**
   * Effects/Actions
   */
  effects: (dispatch) => ({
    async fetchList({ forceSync = false } = {}, rootState) {
      const { tasks = {} } = rootState;
      const { lastSync = {} } = tasks;

      if (lastSync
        && !forceSync
        && moment().isBefore(moment(lastSync).add(config.cacheTimeout.amount, config.cacheTimeout.unit))) {
        return true;
      }

      try {
        const { data } = await Api.get(config.apiEndpoints.tasksGetList);

        return !data
          ? true
          : dispatch.tasks.replace({ data });
      } catch (error) {
        throw HandleErrorMessage(error);
      }
    },
  }),

  /**
   * Reducers
   */
  reducers: {
    replace(state, payload) {
      let list = null;
      const { data } = payload;

      // Loop data array, saving items in a usable and sorted format
      if (data && Array.isArray(data)) {
        list = data.map((item) => transform(item));
      }

      return list
        ? {
          ...state,
          list,
          lastSync: moment().format(),
        }
        : initialState;
    },
  },
};
