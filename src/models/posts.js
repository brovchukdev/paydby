import moment from 'moment';
import initialState from '../store/posts';
import HandleErrorMessage from '../lib/format-error-messages';
import Api from '../lib/api';
import config from '../constants/config';

/**
 * Transform the endpoint data structure into our redux store format
 * @param {object} item
 */
const transform = (item) => ({
  id: item.id,
  name: item.name,
  order: parseInt(item.order, 10),
  subtitle: item.subtitle,
  content: item.content,
  image: item.image,
});

export default {
  namespace: 'posts',

  /**
   * Initial state
   */
  state: initialState,

  /**
   * Effects/Actions
   */
  effects: (dispatch) => ({
    async fetchList({ forceSync = false } = {}, rootState) {
      const { posts = {} } = rootState;
      const { lastSync = {} } = posts;

      if (lastSync
        && !forceSync
        && moment().isBefore(moment(lastSync).add(config.cacheTimeout.amount, config.cacheTimeout.unit))) {
        return true;
      }

      try {
        const { data } = await Api.get(config.apiEndpoints.postsGetList);

        return !data
          ? true
          : dispatch.posts.replace({ data });
      } catch (error) {
        throw HandleErrorMessage(error);
      }
    },
  }),

  /**
   * Reducers
   */
  reducers: {
    /**
     * Replace list in store
     * @param {object} state
     * @param {object} payload
     */
    replace(state, payload) {
      let list = null;
      const { data } = payload;

      // Loop data array, saving items in a usable and sorted format
      if (data && Array.isArray(data)) {
        list = data.map((item) => transform(item)).sort((a, b) => a.order - b.order);
      }

      return list
        ? {
          ...state,
          list,
          lastSync: moment().format(),
        }
        : initialState;
    },
  },
};
