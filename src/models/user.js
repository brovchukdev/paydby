import initialState from '../store/user';
import HandleErrorMessage from '../lib/format-error-messages';
import Api from '../lib/api';
import { errorMessages } from '../constants/messages';
import config from '../constants/config';

const transform = (item) => ({
  id: item.id,
  firstName: item.first_name,
  lastName: item.last_name,
  email: item.email,
  avatar: item.avatar,
});

export default {
  namespace: 'user',

  /**
   * Initial state
   */
  state: initialState,

  /**
   * Effects/Actions
   */
  effects: (dispatch) => ({
    /**
     * Get a user from the API
     * @returns {Promise<object>}
     */
    async fetchData() {
      try {
        const { data } = await Api.get(config.apiEndpoints.userGetData);

        if (!data) {
          throw new Error({ message: errorMessages.user404 });
        }

        const user = transform(data);

        dispatch.user.saveData({ user });
      } catch (error) {
        throw HandleErrorMessage(error);
      }
    },
  }),

  /**
   * Reducers
   */
  reducers: {
    saveData(state, payload) {
      const { user } = payload;

      return user
        ? { ...user }
        : initialState;
    },
  },
};
