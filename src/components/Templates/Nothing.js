import React from 'react';
import PropTypes from 'prop-types';
import { Container } from 'reactstrap';
import { Helmet } from 'react-helmet';
import config from '../../constants/config';
import Footer from '../UI/Footer';

const Template = ({ pageTitle, children }) => (
  <>
    <Helmet>
      <title>{pageTitle}</title>
    </Helmet>

    <div className="sticky">
      <div className="py-3 py-md-5 sticky-content">
        <Container>
          {children}
        </Container>
      </div>
      <Footer />
    </div>
  </>
);

Template.propTypes = {
  pageTitle: PropTypes.string,
  children: PropTypes.element.isRequired,
};

Template.defaultProps = {
  pageTitle: config.appName,
};

export default Template;
