import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {
  Container,
  Row,
  Col,
  Alert,
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  CardText,
  Badge,
} from 'reactstrap';
import Template from '../Templates/Dashboard';
import { Spinner } from '../UI';
import config from '../../constants/config';
import TaskType from '../../prop-types/task';


const List = ({ error, loading, tasks }) => (
  <Template pageTitle={`${config.appName} - Tasks`}>
    <Container>
      <Row>
        <Col>
          {!!error && <Alert color="danger">{error}</Alert>}

          {loading && <Spinner />}
        </Col>
        {tasks && tasks.length === 0 && <p>No Tasks found</p>}

        {tasks && tasks.length > 0 && (
          <Row>
            <Col sm={12}>
              <h1 className="text-center ml-3 mr-3">
                You have
                {' '}
                <strong>
                  {tasks.length}
                </strong>
                {' '}
                requests to catch up on today
              </h1>
            </Col>
            {tasks.map((task) => (
              <Col sm={12} md={6} key={task.id}>
                <Card className="mb-3">
                  <CardBody>
                    <CardTitle><h3>{task.name}</h3></CardTitle>
                    <CardSubtitle><strong>{task.subtitle}</strong></CardSubtitle>
                    <CardText>
                      {task.description.slice(0, 40)}
                      ...
                    </CardText>
                    <Row>
                      <Col sm={12} md={6} className="text-center text-md-left">
                        <Link className="btn btn-info" to={`/tasks/${task.id}`}>{task.name}</Link>
                      </Col>
                      <Col sm={12} md={6} className="text-center text-md-right">
                        {
                          task.tags.length > 0 && task.tags.map((item) => (
                            <Badge key={item} className="mr-1" tag="span">
                              {item}
                            </Badge>
                          ))
                        }
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
              </Col>
            ))}
          </Row>
        )}
      </Row>
    </Container>
  </Template>
);

List.propTypes = {
  error: PropTypes.string,
  loading: PropTypes.bool,
  tasks: PropTypes.arrayOf(TaskType),
};

List.defaultProps = {
  error: null,
  loading: false,
  tasks: [],
};

export default memo(List);
