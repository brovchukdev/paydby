import React from 'react';
import PropTypes from 'prop-types';
import { Link, Redirect } from 'react-router-dom';
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  Alert,
  CardSubtitle,
  CardText,
} from 'reactstrap';
import Template from '../Templates/Dashboard';
import config from '../../constants/config';
import TaskType from '../../prop-types/task';


const Single = ({ error, loading, task }) => {
  if (!loading && !task) return <Redirect to="/404" />;

  return (
    <Template pageTitle={`${config.appName} - Tasks - ${task.name}`}>
      <React.Fragment>
        <Container>
          <Row>
            <Col md="12">
              {!!error && <Alert color="danger">{error}</Alert>}
              {loading && <Alert color="warning">Loading...</Alert>}
            </Col>

            <Col md={{ size: 10, offset: 1 }}>
              <Card className="p-3">
                <CardBody>
                  <CardSubtitle><h2>{task.subtitle}</h2></CardSubtitle>
                  <CardText>{task.description}</CardText>
                </CardBody>
              </Card>
            </Col>
          </Row>

          <Row>
            <Col className="text-center my-5 mb-md-0">
              <Link to="/tasks/" className="btn btn-secondary">Back</Link>
            </Col>
          </Row>
        </Container>
      </React.Fragment>
    </Template>
  );
};

Single.propTypes = {
  task: TaskType,
  error: PropTypes.string,
  loading: PropTypes.bool,
};

Single.defaultProps = {
  error: null,
  loading: false,
  task: {},
};

export default Single;
