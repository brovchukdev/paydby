import React, { memo } from 'react';
import { Link } from 'react-router-dom';
import {
  Container,
  Row,
  Col,
} from 'reactstrap';
import config from '../constants/config';
import UserType from '../prop-types/user';

// Components
import Template from './Templates/Dashboard';
import UserInfo from './User/Info';


const Home = ({ user }) => (
  <Template pageTitle={config.appName}>
    <Container>
      <Row>
        <Col>
          <h1 className="text-center" style={{ color: '#90278b' }}>
            <strong>Welcome, </strong>
            {user.firstName}
          </h1>
          <UserInfo user={user} />
          <div className="row justify-content-around">
            <Link to="/tasks" className="btn btn-info">Tasks</Link>
            <Link to="/posts" className="btn btn-info">Posts</Link>
          </div>
        </Col>
      </Row>
    </Container>
  </Template>
);

Home.propTypes = {
  user: UserType.isRequired,
};

export default memo(Home);
