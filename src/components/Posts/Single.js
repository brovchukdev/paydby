import React from 'react';
import PropTypes from 'prop-types';
import { Link, Redirect } from 'react-router-dom';
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  Alert,
  CardImg,
} from 'reactstrap';
import Template from '../Templates/Dashboard';
import config from '../../constants/config';
import PostType from '../../prop-types/post';


const Single = ({ error, loading, post }) => {
  if (!loading && !post) return <Redirect to="/404" />;

  return (
    <Template pageTitle={`${config.appName} - Posts - ${post.name}`}>
      <React.Fragment>
        <Container>
          <Row>
            <Col md="12">
              {!!error && <Alert color="danger">{error}</Alert>}
              {loading && <Alert color="warning">Loading...</Alert>}
            </Col>

            <Col md={{ size: 10, offset: 1 }}>
              <Card className="p-3">
                {post.image && (
                  <CardImg
                    width="100%"
                    src={post.image}
                    alt={post.name}
                    style={{ maxWidth: 500, maxHeight: 500 }}
                    className="m-auto"
                  />
                )}
                <CardBody>
                  <CardTitle>
                    <h2>{post.subtitle}</h2>
                  </CardTitle>
                  <p>{post.content}</p>
                </CardBody>
              </Card>
            </Col>
          </Row>

          <Row>
            <Col className="text-center my-5 mb-md-0">
              <Link to="/posts/" className="btn btn-secondary">Back</Link>
            </Col>
          </Row>
        </Container>
      </React.Fragment>
    </Template>
  );
};

Single.propTypes = {
  post: PostType,
  error: PropTypes.string,
  loading: PropTypes.bool,
};

Single.defaultProps = {
  error: null,
  loading: false,
  post: {},
};

export default Single;
