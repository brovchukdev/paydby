import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {
  Container,
  Row,
  Col,
  Alert,
  ListGroup,
  ListGroupItem,
} from 'reactstrap';
import Template from '../Templates/Dashboard';
import { Spinner } from '../UI';
import config from '../../constants/config';
import PostType from '../../prop-types/post';


const List = ({ error, loading, posts }) => (
  <Template pageTitle={`${config.appName} - Posts`}>
    <Container>
      <Row>
        <Col>
          {!!error && <Alert color="danger">{error}</Alert>}

          {loading && <Spinner />}

          {posts && posts.length === 0 && <p>No Posts found</p>}

          {posts && posts.length > 0 && (
            <>
              <h1 className="text-center ml-3 mr-3">Here are some blog posts...</h1>
              <ListGroup>
                {posts.map((post) => (
                  <ListGroupItem key={post.id}>
                    <Link to={`/posts/${post.id}`}>{post.name}</Link>
                  </ListGroupItem>
                ))}
              </ListGroup>
            </>
          )}
        </Col>
      </Row>
    </Container>
  </Template>
);

List.propTypes = {
  error: PropTypes.string,
  loading: PropTypes.bool,
  posts: PropTypes.arrayOf(PostType),
};

List.defaultProps = {
  error: null,
  loading: false,
  posts: [],
};

export default memo(List);
