import React from 'react';
import { Jumbotron, ListGroup, ListGroupItem } from 'reactstrap';
import UserType from '../../prop-types/user';

const Info = ({ user }) => {
  const { firstName, lastName, email } = user;

  return (
    <Jumbotron className="user-info">
      <ListGroup className="mb-5">
        <ListGroupItem>
          Name:
          {' '}
          {firstName}
        </ListGroupItem>
        <ListGroupItem>
          Last name:
          {' '}
          {lastName}
        </ListGroupItem>
        <ListGroupItem>
          Email:
          {' '}
          {email}
        </ListGroupItem>
      </ListGroup>
    </Jumbotron>
  );
};


Info.propTypes = {
  user: UserType.isRequired,
};

export default Info;
