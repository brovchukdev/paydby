import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import config from '../../constants/config';

const Footer = () => (
  <footer className="bg-dark py-4">
    <Container>
      <Row>
        <Col>
          <p className="text-center text-secondary">
            &copy;
            {` ${config.appName}. All Rights Reserved.`}
          </p>
        </Col>
      </Row>
    </Container>
  </footer>
);

export default Footer;
