import React from 'react';
import { Spinner as ReactstrapSpinner } from 'reactstrap';

const Spinner = () => <ReactstrapSpinner size="3rem" className="spinner-centered" />;

export default Spinner;
