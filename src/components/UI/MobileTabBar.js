import React from 'react';
import PropTypes from 'prop-types';
import {
  Nav,
  NavItem,
} from 'reactstrap';
import { NavLink, withRouter } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTachometerAlt, faPalette, faMoneyBillWave } from '@fortawesome/free-solid-svg-icons';

const MobileTabBar = () => (
  <div className="mobile-tab-bar d-md-none">
    <Nav fill pills>
      <NavItem>
        <NavLink exact className="nav-link" activeClassName="active" to="/">
          <FontAwesomeIcon icon={faTachometerAlt} />
          <span>Home</span>
        </NavLink>
      </NavItem>
      <NavItem>
        <NavLink className="nav-link" activeClassName="active" to="/tasks/">
          <FontAwesomeIcon icon={faPalette} />
          <span>Tasks</span>
        </NavLink>
      </NavItem>
      <NavItem>
        <NavLink className="nav-link" activeClassName="active" to="/posts/">
          <FontAwesomeIcon icon={faMoneyBillWave} />
          <span>Posts</span>
        </NavLink>
      </NavItem>
    </Nav>
  </div>
);

MobileTabBar.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
};

export default withRouter(MobileTabBar);
