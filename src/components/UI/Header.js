import React, { useState } from 'react';
import {
  Nav,
  Navbar,
  NavItem,
  Collapse,
  NavbarToggler,
} from 'reactstrap';
import { NavLink, Link } from 'react-router-dom';
import Config from '../../constants/config';
import Logo from '../../assets/images/logo.png';


const Header = () => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <header>
      <Navbar dark color="#fff" expand="md">
        <Link to="/" className="navbar-brand" style={{ color: '#90278b' }}>
          <img
            className="header__logo"
            src={Logo}
            alt={Config.appName}
            title={Config.appName}
          />
        </Link>

        <NavbarToggler onClick={() => setIsOpen(!isOpen)} />

        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <NavLink exact className="nav-link" activeClassName="active" to="/">
                <span>Home</span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink className="nav-link" activeClassName="active" to="/tasks/">
                <span>Tasks</span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink className="nav-link" activeClassName="active" to="/posts/">
                <span>Posts</span>
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </header>
  );
};

export default Header;
